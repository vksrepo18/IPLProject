package com.mountblue.main;

import java.io.IOException;
import java.text.ParseException;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;

import com.mountblue.beans.Delivery;
import com.mountblue.beans.Match;
import com.mountblue.beans.MatchResult;
import com.mountblue.data.DeliveryData;
import com.mountblue.data.MatchesData;
import com.mountblue.helper.Economy;

public class App {

	/*
	 * Task 
	 * 1. Number of matches played per year of all the years in IPL. 
	 * 2. Number of matches won of all teams over all the years of IPL. 
	 * 3. For the year 2016 get the extra runs conceded per team. 
	 * 4. For the year 2015 get the top economical bowlers. 
	 * 5. Create your own scenario. 
	 *    Find runs scored by each batsman in entire IPL 
	 *    (sort in descending order of runs)
	 */

	public static void main(String[] args) throws IOException, ParseException {
		// get data
		List<Delivery> deliveries = DeliveryData.getDeliveryData();
		Map<Integer, Match> matches = MatchesData.getMatchesData();

		// task-1
		TreeMap<Integer, Integer> matchesInAllYears = matchesInAllYears(matches);
		System.out.println("1. Number of matches played in all years: ");
		System.out.println(matchesInAllYears);		
		// {2008=58, 2009=57, 2010=60, 2011=73, 2012=74, 2013=76, 2014=60, 2015=59, 2016=60, 2017=59}
		
		// task-2
		HashMap<String, Integer> matchesWonInAllYears = matchesWonInAllYears(matches);
		System.out.println("2. Number of matches won of all teams over all the years of IPL: ");
		System.out.println(matchesWonInAllYears);
		/* {Mumbai Indians=92, Sunrisers Hyderabad=42, Pune Warriors=12, Rajasthan Royals=63, 
		 * Kolkata Knight Riders=77, Royal Challengers Bangalore=73, Gujarat Lions=13, 
		 * Rising Pune Supergiant=10, Kochi Tuskers Kerala=6, Kings XI Punjab=70, 
		 * Deccan Chargers=29, Delhi Daredevils=62, Rising Pune Supergiants=5, Chennai Super Kings=79}
		 * */
		
		// task-3
		int year = 2016;
		HashMap<String, Integer> extraRunsPerTeam = extraRunsPerTeam(matches, deliveries, year);
		System.out.println("3. Extra runs conceded per team in 2016: ");
		System.out.println(extraRunsPerTeam);
		/* {Gujarat Lions=132, Mumbai Indians=102, Sunrisers Hyderabad=124, Kings XI Punjab=83, 
		 * Delhi Daredevils=109, Rising Pune Supergiants=101, Kolkata Knight Riders=130, 
		 * Royal Challengers Bangalore=118}
		 * */
		
		// task-4
		/* What is bowlers economy rate?
		 * If a bowler conceded 50 runs in his 10 overs, his economy-rate stands at 5.00. 
		 * On another note, if bowler B has given 36 runs in 4 overs, his economy rate will be: 36/4 = 9.00
		 * In most circumstances, the lower the economy rate is, the better the bowler is performing.
		 */
		// get bowler and economy rate, sort them based on ascending order of economy rate
		year = 2015;
		Set<Entry<String, Double>> economybowlers = getEconomyBowlers(matches, deliveries, year);
		System.out.println("4. Top economical bowlers: ");
		System.out.println(economybowlers);
		/*
		 * [J Yadav=4.0, RN ten Doeschate=4.0, R Ashwin=5.0, MA Starc=6.0, MC
		 * Henriques=6.0, Parvez Rasool=6.0, S Nadeem=6.0, Z Khan=6.0, A Mishra=7.0, A
		 * Nehra=7.0, AD Russell=7.0, B Kumar=7.0, Bipul Sharma=7.0, CH Morris=7.0, DJ
		 * Muthuswami=7.0, GB Hogg=7.0, Gurkeerat Singh=7.0, HV Patel=7.0, Harbhajan
		 * Singh=7.0, IC Pandey=7.0, Iqbal Abdulla=7.0, JP Duminy=7.0, M Morkel=7.0, M
		 * Vijay=7.0, M de Lange=7.0, MJ McClenaghan=7.0, NM Coulter-Nile=7.0, PP
		 * Chawla=7.0, PV Tambe=7.0, RA Jadeja=7.0, S Aravind=7.0, SK Raina=7.0, SL
		 * Malinga=7.0, SP Narine=7.0, STR Binny=7.0, Sandeep Sharma=7.0, TA Boult=7.0,
		 * A Ashish Reddy=8.0, AD Mathews=8.0, AR Patel=8.0, Ankit Sharma=8.0, BE
		 * Hendricks=8.0, CH Gayle=8.0, CJ Anderson=8.0, D Wiese=8.0, DJ Bravo=8.0, DJ
		 * Hooda=8.0, DJG Sammy=8.0, DR Smith=8.0, DS Kulkarni=8.0, Imran Tahir=8.0, J
		 * Botha=8.0, J Suchith=8.0, KV Sharma=8.0, MM Sharma=8.0, P Kumar=8.0, P
		 * Negi=8.0, P Suyal=8.0, R Dhawan=8.0, R Vinay Kumar=8.0, RS Bopara=8.0, SR
		 * Watson=8.0, Shakib Al Hasan=8.0, TG Southee=8.0, UT Yadav=8.0, YS Chahal=8.0,
		 * Yuvraj Singh=8.0, Anureet Singh=9.0, DW Steyn=9.0, GJ Maxwell=9.0, JA
		 * Morkel=9.0, JP Faulkner=9.0, KA Pollard=9.0, MG Johnson=9.0, NLTC Perera=9.0,
		 * PJ Cummins=9.0, R Bhatia=9.0, AN Ahmed=10.0, GS Sandhu=10.0, HH Pandya=10.0,
		 * J Theron=10.0, JD Unadkat=10.0, R Tewatia=10.0, RG More=10.0, V Kohli=10.0,
		 * VR Aaron=10.0, YK Pathan=10.0, I Sharma=11.0, JJ Bumrah=11.0, Karanveer
		 * Singh=11.0, PP Ojha=11.0, S Gopal=11.0, SA Abbott=11.0, AB Dinda=12.0, BB
		 * Sran=12.0, Azhar Mahmood=13.0, SN Thakur=13.0, KC Cariappa=14.0, Shivam
		 * Sharma=14.0]
		 */

		// task-5
		// Find total runs scored by a batsman in entire IPL
		// then sort it based on run descending order
		// If runs are same then sort based on name ascending order
		Set<Entry<String, Integer>> batsman = getHighScoredInOverBatsman(deliveries);
		System.out.println("5. Runs scored by batsman in entire IPL: ");
		System.out.println(batsman);
		/* [V Kohli=2665, CH Gayle=2546, DA Warner=2527, SK Raina=2484, G Gambhir=2387, 
		 * RG Sharma=2344, RV Uthappa=2301, S Dhawan=2062, AB de Villiers=1980, MS Dhoni=1977, 
		 * BB McCullum=1789, YK Pathan=1688, AM Rahane=1682, V Sehwag=1679, KD Karthik=1591, 
		 * SR Watson=1586, DR Smith=1565, M Vijay=1534, Yuvraj Singh=1529, AC Gilchrist=1497, 
		 * JH Kallis=1476, SE Marsh=1468, PA Patel=1454, SR Tendulkar=1454, KA Pollard=1362, 
		 * AT Rayudu=1296, R Dravid=1257, MK Pandey=1247, MEK Hussey=1103, DPMD Jayawardene=1058, 
		 * AJ Finch=1018, JP Duminy=980, KC Sangakkara=957, NV Ojha=923, SPD Smith=922, 
		 * SC Ganguly=918, RA Jadeja=906, MK Tiwary=869, WP Saha=835, DA Miller=827, 
		 * GJ Maxwell=814, BJ Hodge=789, SV Samson=789, TM Dilshan=747, ML Hayden=738, 
		 * S Badrinath=738, DJ Bravo=734, LMP Simmons=733, F du Plessis=714, DJ Hussey=685, 
		 * SS Tiwary=675, IK Pathan=647, Mandeep Singh=644, M Vohra=619, A Symonds=615, 
		 * KK Nair=604, Y Venugopal Rao=592, HH Gibbs=579, LRPL Taylor=579, KP Pietersen=554, 
		 * MS Bisla=539, MC Henriques=534, ST Jayasuriya=519, EJG Morgan=516, JA Morkel=505, 
		 * SS Iyer=504, MA Agarwal=483, Harbhajan Singh=478, CL White=476, GC Smith=469, 
		 * KM Jadhav=462, Q de Kock=456, STR Binny=445, KL Rahul=432, TL Suman=402, 
		 * AD Mathews=391, JD Ryder=390, AD Russell=386, AM Nayar=380, GJ Bailey=344, 
		 * PC Valthaty=343, CJ Anderson=337, SA Yadav=330, AR Patel=317, OA Shah=301, 
		 * JP Faulkner=292, PP Chawla=292, RR Pant=290, CH Morris=281, KH Pandya=277,
		 * N Rana=277, RS Bopara=277, M Manhas=275, HM Amla=271, SA Asnodkar=270, 
		 * JC Buttler=262, CA Pujara=261, S Sohal=258, Shakib Al Hasan=258, NLTC Perera=253, 
		 * CA Lynn=252, HH Pandya=244, AP Tare=238, JR Hopes=235, RA Tripathi=228, P Kumar=227, 
		 * MJ Lumb=218, Azhar Mahmood=216, DT Christian=213, MV Boucher=213, P Negi=211, 
		 * KS Williamson=210, LR Shukla=210, Ishan Kishan=205, J Botha=205, A Mishra=202, 
		 * JEC Franklin=201, SP Narine=200, Gurkeerat Singh=199, RN ten Doeschate=188, 
		 * AL Menaria=185, MD Mishra=184, DB Ravi Teja=180, BA Stokes=174, UBT Chand=173, 
		 * KV Sharma=170, VVS Laxman=167, DB Das=166, DJ Hooda=166, R Vinay Kumar=166, 
		 * DJG Sammy=164, SP Goswami=161, LA Pomersbach=160, M Kaif=154, R Sathish=153, 
		 * B Chipli=152, R Bhatia=151, SM Katich=150, Salman Butt=143, GH Vihari=142, 
		 * SW Billings=142, A Ashish Reddy=138, Y Nagar=136, YV Takawale=135, SP Fleming=133,
		 * SK Warne=128, K Goel=127, BJ Rohrer=124, CM Gautam=120, MR Marsh=118, FY Fazal=117, 
		 * Bipul Sharma=113, MJ Guptill=113, AA Jhunjhunwala=112, PD Collingwood=112, 
		 * R Ashwin=108, MF Maharoof=107, DH Yagnik=103, RE van der Merwe=102, AS Raut=101,
		 * MG Johnson=97, MN Samuels=97, S Vidyut=97, AC Voges=96, AC Blizzard=95, TM Head=94,
		 * S Anirudha=93, DW Steyn=92, MN van Wyk=86, AB Agarkar=85, Niraj Patel=85, 
		 * R McLaren=85, Sachin Baby=85, SN Khan=84, B Kumar=83, BCJ Cutting=83, Kamran Akmal=83,
		 * MP Stoinis=83, N Saini=82, UT Khawaja=82, P Dogra=81, R Dhawan=80, PA Reddy=78, 
		 * SM Pollock=78, DJ Jacobs=76, DJ Harris=74, AB McDonald=73, CJ Ferguson=71, W Jaffer=71, 
		 * B Lee=67, KK Cooper=67, BB Samantray=64, D Wiese=64, DL Vettori=64, LJ Wright=64, 
		 * Misbah-ul-Haq=64, CR Brathwaite=63, SD Chitnis=63, MS Gony=61, M Morkel=60, RE Levi=59, 
		 * RJ Quiney=58, Z Khan=58, RJ Harris=57, RT Ponting=57, S Rana=57, AP Majumdar=54, 
		 * Ankit Sharma=54, MA Starc=54, MC Juneja=54, PR Shah=54, V Shankar=54, C de Grandhomme=53, 
		 * LA Carseldine=53, MM Sharma=52, M Kartik=49, MJ Clarke=49, UT Yadav=49, Iqbal Abdulla=48, 
		 * RR Sarwan=48, R Sharma=47, SB Styris=47, DS Kulkarni=46, MJ McClenaghan=46, AA Bilakhia=45, 
		 * JDP Oram=45, TR Birt=45, A Flintoff=44, JJ Roy=42, SL Malinga=42, AG Paunikar=40, 
		 * Shahid Afridi=40, WPUJC Vaas=39, IR Jaggi=38, PJ Cummins=38, A Chopra=37, RR Powar=37, 
		 * Anirudh Singh=36, M Rawat=36, Mohammad Hafeez=36, WA Mota=36, B Akhil=35, M Klinger=35, 
		 * Umar Gul=35, WD Parnell=35, AS Yadav=33, J Suchith=33, RV Gomez=33, K Rabada=32, R Rampaul=32,
		 * S Narwal=31, VR Aaron=31, L Ronchi=30, S Sreesanth=30, AD Mascarenhas=29, AJ Tye=29, 
		 * AUK Pathan=28, S Aravind=28, JDS Neesham=27, NM Coulter-Nile=26, TG Southee=26, C Munro=25, 
		 * RJ Peterson=25, Shoaib Malik=25, A Mithun=24, A Nehra=24, KB Arun Karthik=24, CR Woakes=23, 
		 * MM Patel=23, RR Rossouw=23, Yashpal Singh=23, DJ Thornely=22, AB Barath=21, SK Trivedi=21, 
		 * ER Dwivedi=20, R Tewatia=20, SB Bangar=20, HV Patel=19, LPC Silva=19, B Sumanth=18, BR Dunk=18, 
		 * J Arunkumar=18, T Taibu=18, Y Gnaneswara Rao=18, A Kumble=17, AB Dinda=17, KW Richardson=17, 
		 * Mohammed Shami=17, PJ Sangwan=17, SP Jackson=17, Sohail Tanvir=17, AN Ahmed=16, JO Holder=16,
		 * Joginder Sharma=16, MS Wade=16, D Salunkhe=15, L Balaji=15, P Sahu=15, S Sriram=15, 
		 * SB Jakati=15, AP Dole=14, I Sharma=14, S Nadeem=14, Sandeep Sharma=14, JJ Bumrah=13, 
		 * JM Kemp=13, NJ Maddinson=13, RP Singh=13, S Chanderpaul=13, SW Tait=13, VY Mahesh=13, 
		 * C Madan=12, Imran Tahir=12, Sunny Singh=12, BJ Haddin=11, Basil Thampi=11, CA Ingram=11, 
		 * Harmeet Singh=11, M Muralitharan=11, NS Naik=11, DR Martyn=10, GB Hogg=10, Harpreet Singh=10, 
		 * J Syed Mohammad=10, MDKJ Perera=10, S Gopal=10, AD Nath=9, AR Bawne=9, Anureet Singh=9, 
		 * BMAJ Mendis=9, Kuldeep Yadav=9, T Kohli=9, TD Paine=9, AC Thomas=8, DE Bollinger=8, K Upadhyay=8, 
		 * Karanveer Singh=8, M Ntini=8, NL McCullum=8, PV Tambe=8, SA Abbott=8, UA Birla=8, Vishnu Vinod=8, 
		 * YS Chahal=8, A Choudhary=7, AG Murtaza=7, CK Kapugedera=7, CK Langeveldt=7, DAJ Bracewell=7, 
		 * DL Chahar=7, DS Lehmann=7, DT Patil=7, F Behardien=7, MA Khote=7, RR Raje=7, S Kaul=7, 
		 * AA Chavan=6, BA Bhatt=6, DM Bravo=6, PP Ojha=6, S Badree=6, T Thushara=6, TM Srivastava=6, 
		 * X Thalaivan Sargunam=6, AA Noffke=5, AN Ghosh=5, CJ McKay=5, D du Preez=5, DNT Zoysa=5, 
		 * GR Napier=5, J Yadav=5, JJ van der Wath=5, KAJ Roach=5, Parvez Rasool=5, R Bishnoi=5, 
		 * R Shukla=5, SN Thakur=5, Swapnil Singh=5, T Henderson=5, VH Zol=5, Washington Sundar=5, 
		 * A Mukund=4, DP Nannes=4, Mohammad Ashraful=4, PSP Handscomb=4, FH Edwards=3, J Theron=3, 
		 * JE Taylor=3, KJ Abbott=3, Kamran Khan=3, Pankaj Singh=3, SM Harwood=3, SMSM Senanayake=3, 
		 * A Chandila=2, A Singh=2, AS Rajpoot=2, B Laughlin=2, D Kalyankrishna=2, H Das=2, Jaskaran Singh=2, 
		 * KC Cariappa=2, Mohammad Nabi=2, R Ninan=2, RS Gavaskar=2, S Randiv=2, SB Joshi=2, SS Shaikh=2, 
		 * TS Mills=2, A Uniyal=1, A Zampa=1, AF Milne=1, BAW Mendis=1, BE Hendricks=1, CJ Jordan=1, I Malhotra=1, 
		 * JD Unadkat=1, Mohammad Asif=1, P Awana=1, PM Sarvesh Kumar=1, Rashid Khan=1, S Ladda=1, S Tyagi=1, 
		 * Shoaib Ahmed=1, Shoaib Akhtar=1, TA Boult=1, VRV Singh=1, VS Malik=1, Younis Khan=1, Abdur Razzak=0, 
		 * Ankit Soni=0, BB Sran=0, C Nanda=0, CRD Fernando=0, DJ Muthuswami=0, DP Vijaykumar=0, GD McGrath=0, 
		 * IC Pandey=0, KMDN Kulasekara=0, KP Appanna=0, L Ablish=0, LH Ferguson=0, M Ashwin=0, M de Lange=0, 
		 * Mashrafe Mortaza=0, ND Doshi=0, NJ Rimmington=0, P Parameswaran=0, RD Chahar=0, RG More=0, 
		 * RR Bhatkal=0, RS Sodhi=0, S Kaushik=0, SB Wagh=0, SE Bond=0, SJ Srivastava=0, Shivam Sharma=0, 
		 * Sunny Gupta=0, U Kaul=0, V Pratap Singh=0, VS Yeligati=0, YA Abdulla=0]
		 * */
	}

	/* TASK - 5 */
	// method to find batsman scored high in entire IPL
	private static Set<Entry<String, Integer>> getHighScoredInOverBatsman(List<Delivery> deliveries) {
		Iterator<Delivery> itr = null;
		Delivery delivery = null;
		HashMap<String, Integer> batsman = null;
		String batsmanName = null;
		String currentbatsman = null;
		int run = 0;
		int totalrun = 0;
		SortedSet<Entry<String, Integer>> sortedset = null;
		
		// initialize values
		batsman = new HashMap<String, Integer>();
		
		// iterate through values
		itr = deliveries.iterator();
		// for first record
		delivery = itr.next();
		currentbatsman = delivery.getOver().getBall().getBatsman();
		totalrun = delivery.getOver().getBall().getTotal_runs();
		// for next records
		while(itr.hasNext()) {
			delivery = itr.next();
			batsmanName = delivery.getOver().getBall().getBatsman();
			if(currentbatsman.equals(batsmanName)) {
				run = delivery.getOver().getBall().getTotal_runs();
				totalrun += run;
			} else {
				if(batsman.containsKey(currentbatsman)) {
					batsman.put(currentbatsman, batsman.get(currentbatsman) + totalrun);
				} else {
					batsman.put(currentbatsman, totalrun);
				}
				
				currentbatsman = batsmanName;
				totalrun = 0;
			}
		}
		
		
		sortedset = new TreeSet<Entry<String, Integer>>(
				  new Comparator<Entry<String, Integer>>() {
					@Override
					public int compare(Entry<String, Integer> o1, Entry<String, Integer> o2) {
						if(o1.getValue() > o2.getValue()) return -1;
						else if(o1.getValue() < o2.getValue()) return +1;
						// if scores are same then sort based on name (ascending)
						return o1.getKey().compareTo(o2.getKey());
					}  
				  });
		
		// insert elements to sortedset
		sortedset.addAll(batsman.entrySet());
		return sortedset;
	}

	/* TASK - 4 */
	private static Set<Entry<String, Double>> getEconomyBowlers(Map<Integer, Match> matches, List<Delivery> deliveries,
			int year) {
		HashMap<String, Double> economybowlers = null;
		int[] matchId = null;
		SortedSet<Entry<String, Double>> sortedset = null;

		// get start match_id and end match_id for year 2015
		matchId = getStartEndMatchId(matches, year); // [518, 576]

		// find bowlers and its economy rate
		economybowlers = findEconomyRates(deliveries, matchId);

		sortedset = new TreeSet<Entry<String, Double>>(new Comparator<Entry<String, Double>>() {
			@Override
			public int compare(Entry<String, Double> e1, Entry<String, Double> e2) {
				// first sort based on economy rates
				if (e1.getValue() > e2.getValue())
					return +1;
				else if (e1.getValue() < e2.getValue())
					return -1;
				else {
					// if economy rates are same then sort in ascending of name
					return e1.getKey().compareTo(e2.getKey());
				}
			}
		});

		sortedset.addAll(economybowlers.entrySet());

		return sortedset;
	}

	/* TASK - 4.2 */
	// method to find economy rates
	private static HashMap<String, Double> findEconomyRates(List<Delivery> deliveries, int[] matchId) {
		HashMap<String, Double> result = null;
		HashMap<String, Economy> economybowlers = null;
		Delivery delivery = null;
		Economy economy = null;
		Iterator<Delivery> itr = null;
		int currentMatchId = 0;
		String bowler = null;
		int run = 0;
		int balls = 0;
		Set<Entry<String, Economy>> set = null;
		Entry<String, Economy> entry = null;
		Iterator<Entry<String, Economy>> itrEntry = null;

		// get matchId from list and work with only those who belongs to matchId[0] to
		// matchId[1]
		// get bowler name => key
		// get total_runs, total balls, find economy rate => value
		economybowlers = new HashMap<String, Economy>();
		itr = deliveries.iterator();
		while (itr.hasNext()) {
			delivery = itr.next();
			currentMatchId = delivery.getMatch_id();

			if (currentMatchId >= matchId[0] && currentMatchId <= matchId[1]) {
				bowler = delivery.getOver().getBall().getBowler();
				run = delivery.getOver().getBall().getTotal_runs();
				if (economybowlers.containsKey(bowler)) {
					economy = economybowlers.get(bowler);
					economy.setBalls(economy.getBalls() + 1);
					economy.setRuns(economy.getRuns() + run);
					economybowlers.put(bowler, economy);
				} else {
					economybowlers.put(bowler, new Economy(run));
				}
			}
		}

		// iterate economy bowlers and find economyRate and store it to result
		// use Comparator to sort result
		result = new HashMap<String, Double>();
		set = economybowlers.entrySet();
		itrEntry = set.iterator();
		while (itrEntry.hasNext()) {
			entry = itrEntry.next();
			bowler = entry.getKey();
			economy = entry.getValue();

			// find economy rate
			run = economy.getRuns();
			balls = economy.getBalls();
			economy.setEconomyrate(run / (balls / 6));

			// store it to result
			result.put(bowler, economy.getEconomyrate());
		}

		return result;
	}

	/* TASK - 1 */
	// method to return number of matches played per year of all the years in IPL in
	// sorted order
	private static TreeMap<Integer, Integer> matchesInAllYears(Map<Integer, Match> matches) {
		TreeMap<Integer, Integer> totalmatches = null; // [year=matches]
		Collection<Match> col = null;
		Iterator<Match> itr = null;
		Match match = null;
		int sum = 0;
		int session = 0;

		totalmatches = new TreeMap<Integer, Integer>();
		col = matches.values();
		itr = col.iterator();
		// iterate
		while (itr.hasNext()) {
			match = itr.next();
			session = match.getSeason();
			if (totalmatches.containsKey(session)) {
				sum = totalmatches.get(session);
				totalmatches.put(session, sum + 1);
			} else {
				totalmatches.put(session, 1);
			}
		}

		return totalmatches;
	}

	/* TASK - 2 */
	// Method to find number of matches won of all teams over all the years of IPL.
	private static HashMap<String, Integer> matchesWonInAllYears(Map<Integer, Match> matches) {
		HashMap<String, Integer> wonmatches = null;
		Match match = null;
		MatchResult matchresult = null;
		Collection<Match> col = null;
		Iterator<Match> itr = null;
		String winner = null;
		int won = 0;

		wonmatches = new HashMap<String, Integer>();
		col = matches.values();
		itr = col.iterator();
		while (itr.hasNext()) {
			match = itr.next();
			winner = match.getWinner();
			matchresult = match.getResult();
			if (matchresult == MatchResult.NO_RESULT) {
				continue;
			}
			if (wonmatches.containsKey(winner)) {
				won = wonmatches.get(winner);
				wonmatches.put(winner, won + 1);
			} else {
				wonmatches.put(winner, 1);
			}
		}
		return wonmatches;
	}

	/* TASK - 3 */
	// method to get the extra runs conceded per team for the given year
	private static HashMap<String, Integer> extraRunsPerTeam(Map<Integer, Match> matches, List<Delivery> deliveries,
			int year) {
		HashMap<String, Integer> extraruns = null;
		int[] matchId = null;

		// get start match_id and end match_id for year 2016
		matchId = getStartEndMatchId(matches, year); // [577, 636]

		// pass deliveries and matchId to calculate extraruns
		extraruns = findExtraRuns(deliveries, matchId);
		return extraruns;
	}

	/* TASK - 3.2 */
	// method to return extra runs for given match id range per team-wise
	private static HashMap<String, Integer> findExtraRuns(List<Delivery> deliveries, int[] matchId) {
		HashMap<String, Integer> extraruns = null;
		Iterator<Delivery> itr = null;
		Delivery delivery = null;
		int extra = 0;
		int currentMatchId = 0;
		String battingTeam = null;
		int run = 0;

		// get match id and work on it only when it belongs to matchId[0] to matchId[1]
		// get batting team name => key
		// get extra runs => value
		extraruns = new HashMap<String, Integer>();
		itr = deliveries.iterator();

		while (itr.hasNext()) {
			delivery = itr.next();
			currentMatchId = delivery.getMatch_id();

			if (currentMatchId >= matchId[0] && currentMatchId <= matchId[1]) {
				battingTeam = delivery.getBatting_team();
				extra = delivery.getOver().getBall().getExtra_runs();

				if (extraruns.containsKey(battingTeam)) {
					run = extraruns.get(battingTeam);
					extraruns.put(battingTeam, run + extra);
				} else {
					extraruns.put(battingTeam, extra);
				}
			}
		}

		return extraruns;
	}

	/* TASK - 3.1 && TASK - 4.1 */
	// method to return start and end match id for a given year
	private static int[] getStartEndMatchId(Map<Integer, Match> matches, int year) {
		int[] result = new int[2];
		Set<Entry<Integer, Match>> entry = null;
		Iterator<Entry<Integer, Match>> itr = null;
		Entry<Integer, Match> e = null;
		Match match = null;
		int currentyear = 0;

		entry = matches.entrySet();
		itr = entry.iterator();
		while (itr.hasNext()) {
			e = itr.next();
			match = e.getValue();
			currentyear = match.getSeason();
			if (currentyear == year) {
				// data is already sorted
				if (result[0] == 0)
					result[0] = e.getKey();
				result[1] = e.getKey();
			}
		}

		return result;
	}
}
