package com.mountblue.data;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.mountblue.beans.Ball;
import com.mountblue.beans.Delivery;
import com.mountblue.beans.Over;

public class DeliveryData {

	public static List<Delivery> getDeliveryData() throws IOException {
		// variables
		BufferedReader br = null;
		String str[] = null;
		Delivery delivery = null;
		Over over = null;
		Ball ball = null;
		List<Delivery> deliveries = null;
		int match_id = 0;
		int inning = 0;
		String batting_team = null;
		String bowling_team = null;
		int overno = 0;
		int ballno = 0;
		String batsman = null;
		String non_striker = null;
		String bowler = null;
		boolean is_super_over = false;
		int wide_runs = 0;
		int bye_runs = 0;
		int legbye_runs = 0;
		int noball_runs = 0;
		int penalty_runs = 0;
		int batsman_runs = 0;
		int extra_runs = 0;
		int total_runs = 0;
		String player_dismissed = null;
		String dismissal_kind = null;
		String fielder = null;

		// load deliveries.csv file
		br = new BufferedReader(new FileReader("deliveries.csv"));
		// 1st line is for heading (skip it)
		br.readLine();
		/*
		 * match_id,inning,batting_team,bowling_team,over,ball,batsman,
		 * non_striker,bowler,is_super_over,wide_runs,bye_runs,legbye_runs,
		 * noball_runs,penalty_runs,batsman_runs,extra_runs,total_runs,
		 * player_dismissed,dismissal_kind,fielder
		 */
		
		// create HashMap to store Delivery
		deliveries = new ArrayList<Delivery>();
		
		while(br.ready()) {
			str = br.readLine().split(",");
			
			// read data
			match_id = Integer.parseInt(str[0]);
			inning = Integer.parseInt(str[1]);
			batting_team = str[2];
			bowling_team = str[3];
			overno = Integer.parseInt(str[4]);
			ballno = Integer.parseInt(str[5]);
			batsman = str[6];
			non_striker = str[7];
			bowler = str[8];
			if(str[9].equals("0")) {
				is_super_over = false; 
			} else {
				is_super_over = true; 
			}
			wide_runs = Integer.parseInt(str[10]);
			bye_runs = Integer.parseInt(str[11]);
			legbye_runs = Integer.parseInt(str[12]);
			noball_runs = Integer.parseInt(str[13]);
			penalty_runs = Integer.parseInt(str[14]);
			batsman_runs = Integer.parseInt(str[15]);
			extra_runs = Integer.parseInt(str[16]);
			total_runs = Integer.parseInt(str[17]);
			if(str.length > 18) {
				player_dismissed = str[18];
				dismissal_kind = str[19];
			}
			if(str.length > 20) {
				fielder = str[20];
			}

			// create object 
			delivery = new Delivery();
			over = new Over();
			ball = new Ball();
			
			// assign values
			delivery.setMatch_id(match_id);
			delivery.setInning(inning);
			delivery.setBatting_team(batting_team);
			delivery.setBowling_team(bowling_team);
			over.setOverno(overno);
			over.setIs_super_over(is_super_over);
			ball.setBallno(ballno);
			ball.setBatsman(batsman);
			ball.setNon_striker(non_striker);
			ball.setBowler(bowler);
			ball.setWide_runs(wide_runs);
			ball.setBye_runs(bye_runs);
			ball.setLegbye_runs(legbye_runs);
			ball.setNoball_runs(noball_runs);
			ball.setPenalty_runs(penalty_runs);
			ball.setBatsman_runs(batsman_runs);
			ball.setExtra_runs(extra_runs);
			ball.setTotal_runs(total_runs);
			ball.setPlayer_dismissed(player_dismissed);
			ball.setDismissal_kind(dismissal_kind);
			ball.setFielder(fielder);
			over.setBall(ball);
			delivery.setOver(over);
			
			// store to deliveries ArrayList
			deliveries.add(delivery);
			
		}
		
		// display data size
		// System.out.println(deliveries.size());
		
		// close stream
		br.close();
		
		return deliveries;
	}
}
