package com.mountblue.data;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.mountblue.beans.Match;
import com.mountblue.beans.MatchResult;
import com.mountblue.beans.Toss;

public class MatchesData {

	public static Map<Integer, Match> getMatchesData() throws IOException, ParseException {
		// variables
		BufferedReader br = null;
		String str[] = null;
		Match match = null;
		HashMap<Integer, Match> matches = null;
		SimpleDateFormat sdf = null;
		int match_id = 0;
		int season = 0;
		String city = null;
		Date date = null;
		String team1 = null;
		String team2 = null;
		String toss_winner = null;
		String toss_decision = null;
		String result = null;
		int dl_applied = 0;
		String winner = null;
		int win_by_runs = 0;
		int win_by_wickets = 0;
		String player_of_match = null;
		String venue = null;
		String umpire1 = null;
		String umpire2 = null;
		String umpire3 = null;

		// load matches.csv file
		br = new BufferedReader(new FileReader("matches.csv"));
		// 1st line is for heading (skip it)
		br.readLine();
		/*
		 * id,season,city,date,team1,team2,toss_winner,toss_decision,
		 * result,dl_applied,winner,win_by_runs,win_by_wickets,player_of_match,
		 * venue,umpire1,umpire2,umpire3
		 */

		// create HashMap object
		matches = new HashMap<Integer, Match>();

		// date format
		sdf = new SimpleDateFormat("yyyy-mm-dd");

		// read data, create Match object and store to HashMap
		while (br.ready()) {
			
			// read data
			str = br.readLine().split(",");
			match_id = Integer.parseInt(str[0]);
			season = Integer.parseInt(str[1]);
			city = str[2];
			date = sdf.parse(str[3]);
			team1 = str[4];
			team2 = str[5];
			toss_winner = str[6];
			toss_decision = str[7];
			result = str[8];
			dl_applied = Integer.parseInt(str[9]);
			winner = str[10];
			win_by_runs = Integer.parseInt(str[11]);
			win_by_wickets = Integer.parseInt(str[12]);
			player_of_match = str[13];
			venue = str[14];
			// sometimes 1st, 2nd, or 3rd umpire name is not given
			if (str.length > 15) {
				umpire1 = str[15];
			}
			if (str.length > 16) {
				umpire2 = str[16];
			}
			if (str.length > 17) {
				umpire3 = str[17];
			}

			// create Match object and assign values
			match = new Match();
			match.setId(match_id);
			match.setSeason(season);
			match.setCity(city);
			match.setDate(date);
			match.setTeam1(team1);
			match.setTeam2(team2);
			match.setToss_winner(toss_winner);
			switch (toss_decision) {
			case "field":
				match.setToss_decision(Toss.FIELD);
				break;
			case "bat":
				match.setToss_decision(Toss.BAT);
			}
			switch (result) {
			case "normal":
				match.setResult(MatchResult.NORMAL);
				break;
			case "tie":
				match.setResult(MatchResult.TIE);
				break;
			case "no result":
				match.setResult(MatchResult.NO_RESULT);

			}
			match.setDl_applied(dl_applied);
			match.setWinner(winner);
			match.setWin_by_runs(win_by_runs);
			match.setWin_by_wickets(win_by_wickets);
			match.setPlayer_of_match(player_of_match);
			match.setVenue(venue);
			match.setUmpire1(umpire1);
			match.setUmpire2(umpire2);
			match.setUmpire3(umpire3);

			// add it to HashMap
			matches.put(match_id, match);
		}

		// display data
		// System.out.println(matches);

		// close streams
		br.close();
		
		return matches;
	}

}
