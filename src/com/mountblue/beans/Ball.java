package com.mountblue.beans;

public class Ball {
	// variables
	private int ballno;
	private String batsman;
	private String non_striker;
	private String bowler;

	// runs
	private int wide_runs;
	private int bye_runs;
	private int legbye_runs;
	private int noball_runs;
	private int penalty_runs;
	private int batsman_runs;
	private int extra_runs;
	private int total_runs;

	private String player_dismissed;
	private String dismissal_kind;
	private String fielder;
	
	// getter and setter methods
	public int getBallno() {
		return ballno;
	}
	public void setBallno(int ballno) {
		this.ballno = ballno;
	}
	public String getBatsman() {
		return batsman;
	}
	public void setBatsman(String batsman) {
		this.batsman = batsman;
	}
	public String getNon_striker() {
		return non_striker;
	}
	public void setNon_striker(String non_striker) {
		this.non_striker = non_striker;
	}
	public String getBowler() {
		return bowler;
	}
	public void setBowler(String bowler) {
		this.bowler = bowler;
	}
	public int getWide_runs() {
		return wide_runs;
	}
	public void setWide_runs(int wide_runs) {
		this.wide_runs = wide_runs;
	}
	public int getBye_runs() {
		return bye_runs;
	}
	public void setBye_runs(int bye_runs) {
		this.bye_runs = bye_runs;
	}
	public int getLegbye_runs() {
		return legbye_runs;
	}
	public void setLegbye_runs(int legbye_runs) {
		this.legbye_runs = legbye_runs;
	}
	public int getNoball_runs() {
		return noball_runs;
	}
	public void setNoball_runs(int noball_runs) {
		this.noball_runs = noball_runs;
	}
	public int getPenalty_runs() {
		return penalty_runs;
	}
	public void setPenalty_runs(int penalty_runs) {
		this.penalty_runs = penalty_runs;
	}
	public int getBatsman_runs() {
		return batsman_runs;
	}
	public void setBatsman_runs(int batsman_runs) {
		this.batsman_runs = batsman_runs;
	}
	public int getExtra_runs() {
		return extra_runs;
	}
	public void setExtra_runs(int extra_runs) {
		this.extra_runs = extra_runs;
	}
	public int getTotal_runs() {
		return total_runs;
	}
	public void setTotal_runs(int total_runs) {
		this.total_runs = total_runs;
	}
	public String getPlayer_dismissed() {
		return player_dismissed;
	}
	public void setPlayer_dismissed(String player_dismissed) {
		this.player_dismissed = player_dismissed;
	}
	public String getDismissal_kind() {
		return dismissal_kind;
	}
	public void setDismissal_kind(String dismissal_kind) {
		this.dismissal_kind = dismissal_kind;
	}
	public String getFielder() {
		return fielder;
	}
	public void setFielder(String fielder) {
		this.fielder = fielder;
	}
	
	// toString() to display data
	@Override
	public String toString() {
		return "Ball [ballno=" + ballno + ", batsman=" + batsman + ", non_striker=" + non_striker + ", bowler=" + bowler
				+ ", wide_runs=" + wide_runs + ", bye_runs=" + bye_runs + ", legbye_runs=" + legbye_runs
				+ ", noball_runs=" + noball_runs + ", penalty_runs=" + penalty_runs + ", batsman_runs=" + batsman_runs
				+ ", extra_runs=" + extra_runs + ", total_runs=" + total_runs + ", player_dismissed=" + player_dismissed
				+ ", dismissal_kind=" + dismissal_kind + ", fielder=" + fielder + "]";
	}
}
