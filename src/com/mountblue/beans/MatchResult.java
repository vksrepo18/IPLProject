package com.mountblue.beans;

public enum MatchResult {
	NORMAL, TIE, NO_RESULT;
}
