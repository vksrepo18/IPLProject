package com.mountblue.beans;

public class Delivery {
	// variables
	private int match_id;
	private int inning; // [1-2] 
	private String batting_team;
	private String bowling_team; 
	private Over over; // [1-20]
	
	// setter and getter methods
	public int getMatch_id() {
		return match_id;
	}
	public void setMatch_id(int match_id) {
		this.match_id = match_id;
	}
	public int getInning() {
		return inning;
	}
	public void setInning(int inning) {
		this.inning = inning;
	}
	public String getBatting_team() {
		return batting_team;
	}
	public void setBatting_team(String batting_team) {
		this.batting_team = batting_team;
	}
	public String getBowling_team() {
		return bowling_team;
	}
	public void setBowling_team(String bowling_team) {
		this.bowling_team = bowling_team;
	}
	public Over getOver() {
		return over;
	}
	public void setOver(Over over) {
		this.over = over;
	}
	
	// toString() method to display data
	@Override
	public String toString() {
		return "Delivery [match_id=" + match_id + ", inning=" + inning + ", batting_team=" + batting_team
				+ ", bowling_team=" + bowling_team + ", over=" + over + "]";
	}
}
