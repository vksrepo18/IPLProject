package com.mountblue.beans;

public class Over {
	// variables
	private int overno; // over_number
	private boolean is_super_over;
	private Ball ball;

	// setter and getter methods
	public int getOverno() {
		return overno;
	}

	public void setOverno(int overno) {
		this.overno = overno;
	}

	public Ball getBall() {
		return ball;
	}

	public void setBall(Ball ball) {
		this.ball = ball;
	}

	public boolean isIs_super_over() {
		return is_super_over;
	}

	public void setIs_super_over(boolean is_super_over) {
		this.is_super_over = is_super_over;
	}

	// toString() method to display data
	@Override
	public String toString() {
		return "Over [overno=" + overno + ", is_super_over=" + is_super_over + ", ball=" + ball + "]";
	}
}
