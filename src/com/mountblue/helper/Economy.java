package com.mountblue.helper;

public class Economy {
	private double economyrate;
	private int runs;
	private int balls;

	public Economy(int run) {
		this.runs = run;
		this.balls = 1;
	}

	public double getEconomyrate() {
		return economyrate;
	}

	public void setEconomyrate(double economyrate) {
		this.economyrate = economyrate;
	}

	public int getRuns() {
		return runs;
	}

	public void setRuns(int runs) {
		this.runs = runs;
	}

	public int getBalls() {
		return balls;
	}

	public void setBalls(int balls) {
		this.balls = balls;
	}
}
